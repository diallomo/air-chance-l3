
public class Identifiant {

	private String nomUtilisateur="root";	//Chacun met ce qui lui correspond
	private String mdp="";
	private String urlDatabase="jdbc:mysql://localhost:3306/air_chance?useSSL=false&serverTimezone=UTC";
	
	Identifiant(){}
	
	public String getNomUtilisateur() {
		return nomUtilisateur;
	}

	public String getMdp() {
		return mdp;
	}

	public String getUrlDatabase() {
		return urlDatabase;
	}

	
	
}
